$(document).ready(function () {
	$('#productTable').DataTable({
		"order": [[0, 'desc']]
	});
	$('#categoryTable').DataTable({
		"order": [[0, 'desc']]
	});
	$('#purchaseTable').DataTable({
		"order": [[1, 'desc']]
	});
});