<?php

namespace App\Tests\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CardFunctionalTest extends WebTestCase
{
    public function testShouldDisplayCartPage()
    {
        $client = static::createClient();
        $client->request('GET', '/cart');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Votre Panier');
    }

    public function testShoudlRedirectAfterAddProductInCart()
    {
        $client = static::createClient();

        $productRepository = static::getContainer()->get(ProductRepository::class);
        $testProduct = $productRepository->findOneBy(['slug' => 'test-product']);
        
        $client->request('GET', '/cart/add/'.$testProduct->getId());
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    }

    public function testShoudlRedirectAfterDecrementProductInCart()
    {
        $client = static::createClient();

        $productRepository = static::getContainer()->get(ProductRepository::class);
        $testProduct = $productRepository->findOneBy(['slug' => 'test-product']);
        
        $client->request('GET', '/cart/decrement/'.$testProduct->getId());
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    }

    public function testShoudlRedirectAfterDeleteProductInCart()
    {
        $client = static::createClient();

        $productRepository = static::getContainer()->get(ProductRepository::class);
        $testProduct = $productRepository->findOneBy(['slug' => 'test-product']);
        
        $client->request('GET', '/cart/delete/'.$testProduct->getId());
        
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    }

    public function testShoudlRedirectAfterProductNotExistInCart()
    {
        $client = static::createClient();

        $client->request('GET', '/cart/add/0');
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);

        $client->request('GET', '/cart/decrement/0');
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);

        $client->request('GET', '/cart/delete/0');
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }
}
