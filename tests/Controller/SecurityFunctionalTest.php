<?php

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityFunctionalTest extends WebTestCase
{
    public function testShouldDisplayLoginPage(): void
    {
        $client = static::createClient();
        $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Connexion');
        $this->assertSelectorNotExists('alert alert-danger');
    }

    public function testLoginPageWithBadCredential(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Connexion')->form([
            'email' => 'fake@gmail.com',
            'password' => 'fakePass'
        ]);

        $client->submit($form);

        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertSelectorExists('.alert.alert-danger');
    }

    public function testLoginPageSuccessFull(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Connexion')->form([
            'email' => 'admin@gmail.com',
            'password' => 'password'
        ]);

        $client->submit($form);

        $this->assertResponseRedirects('/');
        $client->followRedirect();

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    // public function testShouldDisplayLogoutRedirect()
    // {
    //     $client = static::createClient();
    //     $client->request('GET', '/logout');

    //     $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    // }
}
