<?php

namespace App\Tests\Controller;

use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ProductFunctionalTest extends WebTestCase
{
    public function testShouldDisplayOneProductPage(): void
    {
        $client = static::createClient();
        $client->request('GET', '/category-test/test-product');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('p', 'Description product Test');
    }

    public function testShouldDisplayOneProductNotExist(): void
    {
        $client = static::createClient();
        $client->request('GET', '/category-test/test-product-not-exist');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function testAccessCreateProductLoggedInAdmin()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');

        $client->loginUser($testUser);

        $client->request('GET', '/admin/product/create');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Nouveau produit");
    }

    public function testAccessEditingProductLoggedInAdmin()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');

        $client->loginUser($testUser);

        $productRepository = static::getContainer()->get(ProductRepository::class);
        $testProduct = $productRepository->findOneBy(['slug' => 'test-product']);

        $client->request('GET', '/admin/product/'.$testProduct->getId().'/edit');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Edition du produit : product Test");
    }

    public function testAccessEditingProductNotExistLoggedInAdmin()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');
        
        $client->loginUser($testUser);
        $client->request('GET', '/admin/product/fake/edit');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }
}
