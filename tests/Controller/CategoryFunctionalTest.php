<?php

namespace App\Tests\Controller;

use App\DataFixtures\AppFixtures;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CategoryFunctionalTest extends WebTestCase
{
    public function testShouldDisplayCategoryPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/category-test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Catégorie : category Test');
    }

    public function testShouldDisplayOneCategoryNotExist(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/category-test-not-exist');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function testAccessCreateCategoryLoggedInAdmin()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');

        $client->loginUser($testUser);

        $client->request('GET', '/admin/category/create');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Création d'une catégorie");
    }

    public function testAccessEditingCategoryLoggedInAdmin()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');

        $client->loginUser($testUser);

        $categoryRepository = static::getContainer()->get(CategoryRepository::class);
        $testCategory = $categoryRepository->findOneBy(['slug' => 'category-test']);

        $client->request('GET', '/admin/category/'.$testCategory->getId().'/edit');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Edition de la categorie category Test");
    }

    public function testAccessEditingCategoryNotExistLoggedInAdmin()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');
        
        $client->loginUser($testUser);
        $client->request('GET', '/admin/category/fake/edit');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    // public function testAccessDeleteCategoryLoggedInAdmin()
    // {
    //     $client = static::createClient();
    //     $crawler = $client->request('GET', '/admin');
        
    //     $userRepository = static::getContainer()->get(UserRepository::class);
    //     $testUser = $userRepository->findOneByEmail('admin@gmail.com');

    //     $client->loginUser($testUser);

    //     $categoryRepository = static::getContainer()->get(CategoryRepository::class);
    //     $testCategory = $categoryRepository->findOneBy(['slug' => 'category-test']);
    
    //     $client->request('POST', '/admin/category/'.$testCategory->getId().'/delete');
    //     $this->assertResponseIsSuccessful();
    //     //$this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    // }
}
