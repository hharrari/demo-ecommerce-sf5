<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class CategoryEntityTest extends TestCase
{
    public function testIsTrue()
    {
        $category = new Category();
        $user = new User();
        $product = new Product();

        $category->setName('category 1')
        ->setOwner($user)
        ->addProduct($product)
        ->setSlug('test-category-1');

        $this->assertTrue($category->getName() === 'category 1');
        $this->assertTrue($category->getOwner() === $user);
        $this->assertContains($product, $category->getProducts());
        $this->assertTrue($category->getSlug() === 'test-category-1');
    }

    public function testIsFalse()
    {
        $category = new Category();
        $user = new User();
        $product = new Product();

        $category->setName('category 1')
        ->setOwner($user)
        ->addProduct($product)
        ->setSlug('test-category-1');

        $this->assertFalse($category->getName() === 'false@test.com');
        $this->assertFalse($category->getOwner() === new User());
        $this->assertNotContains(new Product(), $category->getProducts());
        $this->assertFalse($category->getSlug() === 'False category');
    }

    public function testIsEmpty()
    {
        $category = new Category();

        $this->assertEmpty($category->getName());
        $this->assertEmpty($category->getProducts());
        $this->assertEmpty($category->getOwner());
        $this->assertEmpty($category->getSlug());
        $this->assertEmpty($category->getId());
    }

    public function testAddGetRemoveProduct()
    {
        $category = new Category();
        $product = new Product();

        $this->assertEmpty($category->getProducts());

        $category->addProduct($product);
        $this->assertContains($product, $category->getProducts());

        $category->removeProduct($product);
        $this->assertEmpty($category->getProducts());
    }
}
