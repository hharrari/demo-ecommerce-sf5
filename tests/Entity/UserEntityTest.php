<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Purchase;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserEntityTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();
        $category = new Category();
        $purchase = new Purchase();

        $user->setEmail('true@test.com')
        ->setRoles(['ROLE_USER'])
        ->setPassword('test')
        ->setFullName('Test User')
        ->addCategory($category)
        ->addPurchase($purchase);

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getRoles() === ['ROLE_USER']);
        $this->assertTrue($user->getPassword() === 'test');
        $this->assertTrue($user->getFullName() === 'Test User');
        $this->assertTrue($user->getId() === null);
        $this->assertTrue($user->getUsername() === 'true@test.com');
        $this->assertTrue($user->getUserIdentifier() === 'true@test.com');
        $this->assertTrue($user->getSalt() === null);
        $this->assertContains($category, $user->getCategories());
        $this->assertContains($purchase, $user->getPurchases());
    }

    public function testIsFalse()
    {
        $user = new User();
        $category = new Category();
        $purchase = new Purchase();

        $user->setEmail('true@test.com')
        ->setRoles(['ROLE_USER'])
        ->setPassword('test')
        ->setFullName('Test User')
        ->addCategory($category)
        ->addPurchase($purchase);

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getRoles() === ['ROLE_FALSE']);
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getFullName() === 'Test False User');
        $this->assertNotContains(new Category(), $user->getCategories());
        $this->assertNotContains(new Purchase(), $user->getPurchases());
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFullName());
        $this->assertEmpty($user->getCategories());
        $this->assertEmpty($user->getPurchases());
        $this->assertEmpty($user->getId());
        $this->assertEmpty($user->eraseCredentials());
    }

    public function testAddGetCategory()
    {
        $user = new User();
        $categories = new Category();

        $this->assertEmpty($user->getCategories());

        $user->addCategory($categories);
        $this->assertContains($categories, $user->getCategories());

        $user->removeCategory($categories);
        $this->assertEmpty($user->getCategories());
    }

    public function testAddGetPurchase()
    {
        $user = new User();
        $purchase = new Purchase();

        $this->assertEmpty($user->getPurchases());

        $user->addPurchase($purchase);
        $this->assertContains($purchase, $user->getPurchases());

        $user->removePurchase($purchase);
        $this->assertEmpty($user->getPurchases());
    }
}
