<?php

namespace App\Tests\Entity;

use App\Entity\Product;
use App\Entity\Purchase;
use App\Entity\PurchaseItem;
use PHPUnit\Framework\TestCase;

class PurchaseItemEntityTest extends TestCase
{
    public function testIsTrue()
    {
        /**
         * @var PurchaseItem
         */
        $purchaseItem = new PurchaseItem();
        $product = new Product();
        $purchase = new Purchase();

        $purchaseItem->setProduct($product)
        ->setPurchase($purchase)
        ->setProductName('product name test')
        ->setProductPrice(2234)
        ->setQuantity(12)
        ->setTotal(123);

        $this->assertTrue($purchaseItem->getProduct() === $product);
        $this->assertTrue($purchaseItem->getPurchase() === $purchase);
        $this->assertTrue($purchaseItem->getProductName() === 'product name test');
        $this->assertTrue($purchaseItem->getProductPrice() === 2234);
        $this->assertTrue($purchaseItem->getQuantity() === 12);
        $this->assertTrue($purchaseItem->getTotal() === 123);
    }

    public function testIsFalse()
    {
        $purchaseItem = new PurchaseItem();
        $product = new Product();
        $purchase = new Purchase();

        $purchaseItem->setProduct($product)
        ->setPurchase($purchase)
        ->setProductName('product name test')
        ->setProductPrice(2234)
        ->setQuantity(12)
        ->setTotal(123);


        $this->assertFalse($purchaseItem->getProduct() === new Product());
        $this->assertFalse($purchaseItem->getPurchase() === new Purchase());
        $this->assertFalse($purchaseItem->getProductName() === 'product false name test');
        $this->assertFalse($purchaseItem->getProductPrice() === 1233);
        $this->assertFalse($purchaseItem->getQuantity() === 222);
        $this->assertFalse($purchaseItem->getTotal() === 3490);
    }

    public function testIsEmpty()
    {
        $purchaseItem = new PurchaseItem();

        $this->assertEmpty($purchaseItem->getProduct());
        $this->assertEmpty($purchaseItem->getPurchase());
        $this->assertEmpty($purchaseItem->getProductName());
        $this->assertEmpty($purchaseItem->getProductPrice());
        $this->assertEmpty($purchaseItem->getQuantity());
        $this->assertEmpty($purchaseItem->getTotal());
        $this->assertEmpty($purchaseItem->getId());
    }
}
