<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\PurchaseItem;
use PHPUnit\Framework\TestCase;

class ProductEntityTest extends TestCase
{
    public function testIsTrue()
    {
        $product = new Product();
        $category = new Category();
        $purchaseItem = new PurchaseItem();

        $product->setName('product 1')
        ->setPrice(123)
        ->setCategory($category)
        ->setMainPicture("https://photo-test.fr")
        ->setShortDescription('Description product')
        ->addPurchaseItem($purchaseItem)
        ->setSlug('test-product-1');

        $this->assertTrue($product->getName() === 'product 1');
        $this->assertTrue($product->getPrice() === 123);
        $this->assertTrue($product->getCategory() === $category);
        $this->assertTrue($product->getMainPicture() === "https://photo-test.fr");
        $this->assertTrue($product->getShortDescription() === 'Description product');
        $this->assertContains($purchaseItem, $product->getPurchaseItems());
        $this->assertTrue($product->getSlug() === 'test-product-1');
    }

    public function testIsFalse()
    {
        $product = new Product();
        $category = new Category();
        $purchaseItem = new PurchaseItem();

        $product->setName('product 1')
        ->setPrice(123)
        ->setCategory($category)
        ->setMainPicture("https://photo-test.fr")
        ->setShortDescription('Description product')
        ->addPurchaseItem($purchaseItem)
        ->setSlug('test-product-1');

        $this->assertFalse($product->getName() === 'product false');
        $this->assertFalse($product->getPrice() === 33.33);
        $this->assertFalse($product->getCategory() === new Category());
        $this->assertFalse($product->getMainPicture() === "https://photo-false.fr");
        $this->assertFalse($product->getShortDescription() === 'Description false product');
        $this->assertNotContains(new PurchaseItem(), $product->getPurchaseItems());
        $this->assertFalse($product->getSlug() === 'test-product-false');
    }

    public function testIsEmpty()
    {
        $product = new Product();

        $this->assertEmpty($product->getName());
        $this->assertEmpty($product->getPrice());
        $this->assertEmpty($product->getCategory());
        $this->assertEmpty($product->getMainPicture());
        $this->assertEmpty($product->getShortDescription());
        $this->assertEmpty($product->getPurchaseItems());
        $this->assertEmpty($product->getSlug());
        $this->assertEmpty($product->getId());
    }

    public function testAddGetPurchaseItem()
    {
        $product = new Product();
        $purchaseItem = new PurchaseItem();

        $this->assertEmpty($product->getPurchaseItems());

        $product->addPurchaseItem($purchaseItem);
        $this->assertContains($purchaseItem, $product->getPurchaseItems());

        $product->removePurchaseItem($purchaseItem);
        $this->assertEmpty($product->getPurchaseItems());
    }
}
