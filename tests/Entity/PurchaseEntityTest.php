<?php

namespace App\Tests\Entity;

use App\Entity\Purchase;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class PurchaseEntityTest extends TestCase
{
    public function testIsTrue()
    {
        $purchase = new Purchase();
        $datetime = new DateTime();
        $user = new User();

        $purchase->setFullName('purchase 1')
        ->setAddress("1 avenue du puchase")
        ->setCity("Paris")
        ->setTotal(300)
        ->setStatus("PAID")
        ->setPurchasedAt($datetime)
        ->setUser($user)
        ->setPostalCode("75000");

        $this->assertTrue($purchase->getFullName() === 'purchase 1');
        $this->assertTrue($purchase->getAddress() === "1 avenue du puchase");
        $this->assertTrue($purchase->getCity() === "Paris");
        $this->assertTrue($purchase->getTotal() === 300);
        $this->assertTrue($purchase->getStatus() === "PAID");
        $this->assertTrue($purchase->getPurchasedAt() === $datetime);
        $this->assertTrue($purchase->prePersit() === null);
        $this->assertTrue($purchase->getUser() === $user);
        $this->assertTrue($purchase->getPostalCode() === "75000");
    }

    public function testIsFalse()
    {
        $purchase = new Purchase();
        $datetime = new DateTime();
        $user = new User();

        $purchase->setFullName('purchase 1')
        ->setAddress("1 avenue du puchase")
        ->setCity("Paris")
        ->setTotal(300)
        ->setStatus("PAID")
        ->setPurchasedAt($datetime)
        ->setPostalCode("75000");

        $this->assertFalse($purchase->getFullName() === 'purchase false');
        $this->assertFalse($purchase->getAddress() === "1 avenue du false puchase");
        $this->assertFalse($purchase->getCity() === "False");
        $this->assertFalse($purchase->getTotal() === 111);
        $this->assertFalse($purchase->getStatus() === "PENDING");
        $this->assertFalse($purchase->getPurchasedAt() === new DateTime());
        $this->assertFalse($purchase->getUser() === new User());
        $this->assertFalse($purchase->getPostalCode() === "27140");
    }

    public function testIsEmpty()
    {
        $purchase = new Purchase();

        $this->assertEmpty($purchase->getFullName());
        $this->assertEmpty($purchase->getAddress());
        $this->assertEmpty($purchase->getCity());
        $this->assertEmpty($purchase->getTotal());
        $this->assertEmpty($purchase->getPostalCode());
        $this->assertEmpty($purchase->getUser());
        $this->assertEmpty($purchase->getId());
        $this->assertEmpty($purchase->prePersit());
    }

    public function testIsNotEmpty()
    {
        $purchase = new Purchase();
        $purchase->prePersit();
        $purchase->preFlush();

        $this->assertTrue($purchase->preFlush() === null);
        $this->assertNotEmpty($purchase->getPurchasedAt());
        $this->assertTrue($purchase->getPurchasedAt() instanceof \DateTime);
    }
}
