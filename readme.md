# Projet démo ecommerce Harrari Hakime

Ce petit projet représente un site e-commerce avec des produits factice et a pour vocation de présenté une partie de mes compétences.
J'utilise ce projet pour expérimenter de nouveau sujets prochainement.
Ce projet est encore en cours de développement ...


## Envirnonnement de développement

### Pré-requis
* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

Pour vérifier les pré-requis ( hors service Docker ) vous pouvez taper la commande suivante de la CLI Symfony :

```bash
symfony check:requirements
```

### Configuration .env 
Pour tester la partie paiement, un compte stripe doit être créé afin de récupérer les clés et les renseignés dans le fichier .env

https://stripe.com/fr

https://support.stripe.com/questions/locate-api-keys-in-the-dashboard

```bash
STRIPE_SECRET_KEY=""
STRIPE_PUBLIC_KEY=""
```
### Configuration .env.test
Attention le port peut être différents, penser à le vérifier en faisant un docker-compose ps et reprendre le numéro de port de la database

```bash
DATABASE_URL=
```

### Lancer l'environnement de développement

```bash
docker-compose up -d
symfony serve -d
```

### Premier lancement : alimenter la base de donnée

```bash
composer install
yarn install
composer generate
```

### Créer une base de données pour les tests

```bash
composer generate-test
```

### Lancer des tests

```bash
php ./vendor/bin/phpunit --testdox
```

### Lancer des tests avec le mode coverage
```bash
php bin/phpunit --coverage-html var/log/test/test-coverage
```
Vous pouvez ensuite ouvrir sur un navigateur le fichier var/log/test/test-coverage/index.html
/!\ vous devez installer l'extention xdebugg et activer le mode coverage dans php.ini

https://xdebug.org/docs/install#pecl

#### Jeux de données

2 Rôles existent : 
* Admin
* User

Pour accéder à l'espace admin 
Login : admin@gmail.com
mot de pass : admin

Pour accéder à l'espace user 
Login : user0@gmail.com
mot de pass : password

#### Routes disponible 

Voici la commande pour connaître vos différentes routes disponibles.
Tous les liens :  /admin/ sont disponible pour le rôle admin.
Pour passer commande, il vous faut être connecté en tant que User.

```bash
symfony console debug:route --show-controllers 
```

#### PHPMYADMIN
* url : http://localhost:8080/
* user : root
* pass : password
