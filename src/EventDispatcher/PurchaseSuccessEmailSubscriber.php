<?php

namespace App\EventDispatcher;

use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Event\PurchaseSuccessEvent;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PurchaseSuccessEmailSubscriber implements EventSubscriberInterface
{
    protected $logger;
    protected $mailer;
    protected $security;

    public function __construct(LoggerInterface $logger, MailerInterface $mailer, Security $security)
    {
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return ['purchase.success' => 'sendSuccessEmail'];
    }

    public function sendSuccessEmail(PurchaseSuccessEvent $purchaseSuccessEvent)
    {
        /**
         * @var User
         */
        $currentUser = $this->security->getUser();

        $purchase = $purchaseSuccessEvent->getPurchase();

        $email = new TemplatedEmail();
        $email->from(new Address("contact@mail.com", "infos de la boutique"))
            ->to(new Address($currentUser->getEmail(), $currentUser->getFullName()))
            ->htmlTemplate('emails/purchase_success.html.twig')
            ->context(
                [
                'purchase' => $purchase,
                'user' => $currentUser
                ]
            )
            //->text("un visiteur à payer la commande n°" . $purchase->getId())
            ->subject("Félicitation votre commande n°" . $purchase->getId() . " a bien été confirmée");

        $this->mailer->send($email);

        $this->logger->info("Email envoyé pour la commande n° " . $purchase->getId());
    }
}
