<?php

namespace App\EventDispatcher;

use Psr\Log\LoggerInterface;
use App\Event\ProductViewCounterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductViewSubscriber implements EventSubscriberInterface
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        return ['product.view' => 'addCounterView'];
    }

    /**
     * TODO : ADD system counter by product
     *
     * @param  ProductViewCounterEvent $productViewEvent
     * @return void
     */
    public function addCounterView(ProductViewCounterEvent $productViewEvent)
    {
        $this->logger->warning("Le produit : '" . $productViewEvent->getProduct()->getName() . "' vient d'être vue ");
    }
}
