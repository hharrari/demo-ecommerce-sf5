<?php

namespace App\Cart;

use App\Entity\Product;

class CartItem
{
    public $product;
    public $quantity;

    /**
     * @param Product $product
     * @param integer $quantity
     */
    public function __construct(Product $product, int $quantity)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * Return total by item
     *
     * @return integer
     */
    public function getTotal(): int
    {
        return $this->product->getPrice() * $this->quantity;
    }
}
