<?php

namespace App\Cart;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\RequestStack;

class CartService
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @param SessionInterface  $session
     * @param ProductRepository $productRepository
     */
    public function __construct(RequestStack $requestStack, ProductRepository $productRepository)
    {
        $this->requestStack = $requestStack;
        $this->productRepository = $productRepository;
    }

    /**
     * Return current sesssion
     */
    public function session()
    {
        return $this->requestStack->getSession();
    }

    /**
     * Recupère le panier dans la sesssion
     *
     * @return array
     */
    protected function getCart(): array
    {
        return $this->session()->get('cart', []);
    }


    /**
     * Sauvegarde la nouvelle état du panier dans la sesssion
     *
     * @return void
     */
    protected function saveCart(array $cart)
    {
        return $this->session()->set('cart', $cart);
    }

    public function empty()
    {
        $this->saveCart([]);
    }

    /**
     * Ajout un produit au panier
     *
     * @param integer $id
     * @return void
     */
    public function add(int $id)
    {
        // 1 . Retrouver le panier dans la session (sous forme de tableau)
        // 2 . Si il n'éxiste pas encore, alors prendre un tableau vide
        $cart = $this->getCart();

        // 3 . Vois si le produit ($id) existe déjà dans le tableau --- exemple : [Id => Quantité, 12 => 5, 29 => 2 ]
        // 4 . Si c'est le cas, simplement augmenter la quantité
        // 5 . Sinon, ajouter le produite avec la quantité 1
        if (!array_key_exists($id, $cart)) {
            $cart[$id] = 0;
        }

        $cart[$id]++;

        // 6 .  Enregistrer le tableau mis à jour dans la session
        $this->saveCart($cart);
    }


    /**
     * Retirer une quantité au panier
     *
     * @param  integer $id
     * @return void
     */
    public function decrement(int $id)
    {
        $cart = $this->getCart();

        if (!array_key_exists($id, $cart)) {
            return;
        }
        // Soit le produit est à 1 alors supprimer le produit
        if ($cart[$id] === 1) {
            $this->remove($id);
            return;
        }

        $cart[$id]--;

        $this->saveCart($cart);
    }


    /**
     * Supprime un produit du panier
     *
     * @param integer $id
     * @return void
     */
    public function remove(int $id)
    {
        $cart = $this->getCart();

        unset($cart[$id]);

        $this->saveCart($cart);
    }


    /**
     * Get Total price cart
     *
     * @return integer
     */
    public function getTotal(): int
    {
        $total = 0;

        foreach ($this->getCart() as $id => $quantity) {
            $product = $this->productRepository->find($id);
            if (!$product) {
                continue;
            }
            $total += $product->getPrice() * $quantity;
        }
        return $total;
    }


    /**
     * Get detailed Items in cart
     *
     * @return CartItem[]
     */
    public function getdetailedCartItems(): array
    {
        $detailedCart = [];
        foreach ($this->getCart() as $id => $quantity) {
            $product = $this->productRepository->find($id);
            if (!$product) {
                continue;
            }
            $detailedCart[] = new CartItem($product, $quantity);
        }

        return $detailedCart;
    }
}
