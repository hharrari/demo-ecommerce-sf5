<?php

namespace App\Controller;

use App\Cart\CartService;
use App\Form\CartConfimationType;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;
    /**
     * @var CartService
     */
    protected $cartService;


    /**
     * @param ProductRepository $productRepository
     * @param CartService       $cartService
     */
    public function __construct(ProductRepository $productRepository, CartService $cartService)
    {
        $this->productRepository = $productRepository;
        $this->cartService = $cartService;
    }


    /**
     * Affichage du panier
     *
     * @Route("/cart",name="cart_show")
     * @return Response
     */
    public function show(): Response
    {
        $form = $this->createForm(CartConfimationType::class);
        $detailedCart = $this->cartService->getdetailedCartItems();

        $total = $this->cartService->getTotal();
        return $this->render(
            'cart/index.html.twig',
            [
            'items' => $detailedCart,
            'total' => $total,
            'confirmationForm' => $form->createView()
            ]
        );
    }


    /**
     * Ajout d'un produit dans le panier
     *
     * @Route("/cart/add/{id}", name="cart_add", requirements={"id":"\d+"})
     * @param int $id
     * @return Response
     * @param Request $request
     */
    public function add($id, Request $request): Response
    {
        // 0 . Sécurisation : est-ce-que le produit existe ?
        $product = $this->productRepository->find($id);

        if (!$product) {
            throw $this->createNotFoundException("Ce produit $id n'éxiste pas !");
        }

        $this->cartService->add($id);

        $this->addFlash('success', "le produit à bien été ajouté au panier");

        if ($request->query->get('returnToCart')) {
            return $this->redirectToRoute('cart_show');
        }
        return $this->redirectToRoute(
            'product_show',
            [
            'category_slug' => $product->getCategory()->getSlug(),
            'slug' => $product->getSlug()
            ]
        );
    }


    /**
     * Décrément la quantité d'un produit dans le panier
     *
     * @Route("/cart/decrement/{id}" ,name="cart_decrement" ,requirements={"id":"\d+"})
     * @param int $id
     * @return Response
     */
    public function decrement($id): Response
    {
        $product = $this->productRepository->find($id);

        if (!$product) {
            throw $this->createNotFoundException("Ce produit $id n'éxiste pas et ne peut être décrémenté!");
        }

        $this->cartService->decrement($id);
        $this->addFlash("success", "La Quantité à bien été mis à jour");
        return $this->redirectToRoute('cart_show');
    }


    /**
     * Supprime un produit d'un panier
     *
     * @Route("/cart/delete/{id}", name="cart_delete", requirements={"id":"\d+"})
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $product = $this->productRepository->find($id);
        if (!$product) {
            throw $this->createNotFoundException("Le produit $id n'éxiste pas et ne peut pas être supprimé !");
        }

        $this->cartService->remove($id);

        $this->addFlash("success", "Le produit a bien été supprimé du panier");

        return $this->redirectToRoute("cart_show");
    }
}
