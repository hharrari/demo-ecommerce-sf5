<?php

namespace App\Controller\Purchase;

use App\Entity\Purchase;
use App\Cart\CartService;
use App\Event\PurchaseSuccessEvent;
use App\Repository\PurchaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PurchasePaymentSuccessController extends AbstractController
{
    protected $em;
    protected $cartService;
    protected $dispatcher;

    /**
     * @param EntityManagerInterface $em
     * @param CartService $cartService
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        EntityManagerInterface $em,
        CartService $cartService,
        EventDispatcherInterface $dispatcher
    ) {
        $this->em = $em;
        $this->cartService = $cartService;
        $this->dispatcher = $dispatcher;
    }

    //Todo gerer cette partie avec les webhooks de Stripe
    /**
     * @Route("/purchase/terminate/{id}", name="purchase_payment_success")
     * @IsGranted("ROLE_USER")
     * @param int $id
     * @param PurchaseRepository $purchaseRepository
     * @return Response
     */
    public function success($id, PurchaseRepository $purchaseRepository): Response
    {
        $purchase = $purchaseRepository->find($id);

        if (
            !$purchase ||
            $purchase->getUser() !== $this->getUser() ||
            ($purchase && $purchase->getStatus() === Purchase::STATUS_PAID)
        ) {
            $this->addFlash("warning", "la commande n'éxiste pas");
            return $this->redirectToRoute("purchase_index");
        }

        $purchase->setStatus(Purchase::STATUS_PAID);
        $this->em->flush();

        $this->cartService->empty();

        // lance un event qui permet au autres devs de réagir à la prise d'une commande
        $purchaseEvent = new PurchaseSuccessEvent($purchase);
        $this->dispatcher->dispatch($purchaseEvent, 'purchase.success');

        $this->addFlash('success', 'la commande a été payée et confirmé !');

        return $this->redirectToRoute('purchase_index');
    }
}
