<?php

namespace App\Controller\Purchase;

use App\Entity\Purchase;
use App\Repository\PurchaseRepository;
use App\Stripe\StripeService;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class PurchasePaymentController extends AbstractController
{
    protected $stripeService;

    /**
     * @param StripeService $stripeService
     */
    public function __construct(StripeService $stripeService)
    {
        $this->stripeService = $stripeService;
    }

    /**
     * Formulaire de paiement
     *
     * @Route("/purchase/pay/{id}",name="purchase_payment_form")
     * @IsGranted("ROLE_USER")
     * @param [int] $id
     * @param PurchaseRepository $purchaseRepository
     * @return Response
     */
    public function showCardForm($id, PurchaseRepository $purchaseRepository): Response
    {
        $purchase = $purchaseRepository->find($id);

        if (
            !$purchase ||
            $purchase->getUser() !== $this->getUser() ||
            ($purchase && $purchase->getStatus() === Purchase::STATUS_PAID)
        ) {
            return $this->redirectToRoute("cart_show");
        }

        $intent = $this->stripeService->getPaymentIntent($purchase);


        return $this->render(
            'purchase/payment.html.twig',
            [
            'clientSecret' => $intent->client_secret,
            'purchase' => $purchase,
            'stripePublicKey' => $this->stripeService->getPublicKey()
            ]
        );
    }
}
