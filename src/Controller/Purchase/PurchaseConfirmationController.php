<?php

namespace App\Controller\Purchase;

use DateTime;
use App\Entity\Purchase;
use App\Cart\CartService;
use App\Entity\PurchaseItem;
use App\Form\CartConfimationType;
use App\Purchase\PurchasePersister;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PurchaseConfirmationController extends AbstractController
{
    protected $cartService;
    protected $em;
    protected $persister;

    public function __construct(CartService $cartService, EntityManagerInterface $em, PurchasePersister $persister)
    {
        $this->cartService = $cartService;
        $this->em = $em;
        $this->persister = $persister;
    }

    /**
     * Formulaire de comfirmation de commande
     *
     * @Route("/purchase/confirm", name="purchase_confirm")
     * @IsGranted("ROLE_USER"      , message="Vous devez être connecté pour confirmez une commande")
     * @return                     Response
     */
    public function confirm(Request $request): Response
    {
        $form = $this->createForm(CartConfimationType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            $this->addFlash('warning', 'Vous devez remplir le formulaire de confirmation');
            return $this->redirectToRoute('cart_show');
        }

        // Si il y'a pas de produit dans mon panier : degager (cartService)
        $cartItem = $this->cartService->getdetailedCartItems();

        if (count($cartItem) === 0) {
            $this->addFlash('warning', 'Vous ne pouvez pas confirmez une commande sans panier');
            return $this->redirectToRoute('cart_show');
        }

        // Nous allons créer une Purchase
        /**
         * @var Purchase
         */
        $purchase = $form->getData();

        $this->persister->storePurchase($purchase);

        return $this->redirectToRoute(
            'purchase_payment_form',
            [
            'id' => $purchase->getId()
            ]
        );
    }
}
