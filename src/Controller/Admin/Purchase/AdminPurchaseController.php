<?php

namespace App\Controller\Admin\Purchase;

use App\Entity\Purchase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfToken;

class AdminPurchaseController extends AbstractController
{
    /**
     * @var EntityManager $em
     */
    protected $em;

    /**
     * @var CsrfTokenManagerInterface $csrfTokenManagerInterface
     */
    protected $csrfTokenManagerInterface;

    /**
     * Construct function AdminPuchaseController
     *
     * @param EntityManagerInterface $em
     * @param CsrfTokenManagerInterface $csrfTokenManagerInterface
     */
    public function __construct(EntityManagerInterface $em, CsrfTokenManagerInterface $csrfTokenManagerInterface)
    {
        $this->em = $em;
        $this->csrfTokenManagerInterface = $csrfTokenManagerInterface;
    }

    /**
     * Details Purchase
     *  @Route("/admin/purchase/{id}/details", name="admin_purchase_details", requirements={"id": "\d+"})
     * @param Purchase $purchase
     * @return Response
     */
    public function details(Purchase $purchase): Response
    {
        return $this->render('/admin/purchase/details_purchase_html.twig', [
            'purchase' => $purchase
        ]);
    }

    /**
     * Delete purchase
     * @Route("/admin/purchase/{id}/delete", name="admin_purchase_delete", requirements={"id": "\d+"}, methods="POST")
     * @param Purchase $purchase
     * @param Request $request
     */
    public function delete(Purchase $purchase, Request $request)
    {
        if (!$purchase) {
            throw $this->createNotFoundException("La commande demandée n'éxiste pas");
        }

        if (
            $this->csrfTokenManagerInterface->isTokenValid(
                new CsrfToken("delete-purchase", $request->request->get('token'))
            )
        ) {
            $purchaseId = $purchase->getId();
            $this->em->remove($purchase);
            $this->em->flush();

            $this->addFlash('success', "La commande n° " . $purchaseId . " à bien été supprimé");
            return $this->redirectToRoute('admin_home');
        }
    }
}
