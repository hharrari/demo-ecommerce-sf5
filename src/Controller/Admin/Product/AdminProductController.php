<?php

namespace App\Controller\Admin\Product;

use App\Entity\Product;
use App\Form\ProductType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class AdminProductController extends AbstractController
{
    /**
     * @var EntityManager $em
     */
    protected $em;

    /**
     * @var CsrfTokenManagerInterface $csrfTokenManagerInterface
     */
    protected $csrfTokenManagerInterface;

    /**
     * Construct function AdminPuchaseController
     *
     * @param EntityManagerInterface $em
     * @param CsrfTokenManagerInterface $csrfTokenManagerInterface
     */
    public function __construct(EntityManagerInterface $em, CsrfTokenManagerInterface $csrfTokenManagerInterface)
    {
        $this->em = $em;
        $this->csrfTokenManagerInterface = $csrfTokenManagerInterface;
    }

    /**
     * Création d'un produit
     *
     * @Route("/admin/product/create", name="admin_product_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($product);
            $this->em->flush();
            return $this->redirectToRoute(
                'product_show',
                [
                'category_slug' => $product->getCategory()->getSlug(),
                'slug' => $product->getSlug()
                ]
            );
        }

        $formView = $form->createView();

        return $this->render(
            'product/create.html.twig',
            [
            'formView' => $formView
            ]
        );
    }

    /**
     * Modification d'un produit
     *
     * @Route("/admin/product/{id}/edit", name="admin_product_edit")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function edit(Product $product, Request $request): Response
    {

        // exemple pour ne sélectionner que des propriétés d'une entity qui font partie d'un groupe
        //( ex :  dans l'entity mettre à coté des annotations ,groups={"white-price"})
        //et ont peut ajouter le groupe par defaut en l'ajoutant dans le 'validation_groups' => ["with-price","default"]
        // $form = $this->createForm(ProductType::class, $product, [
        //     'validation_groups' => 'with-price'
        // ]);
        if (!$product) {
            throw $this->createNotFoundException("le produit demandée n'éxiste pas");
        }

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', "Le produit " . $product->getName() . " à bien été modifié");
            return $this->redirectToRoute('admin_home');
        }

        $formView = $form->createView();

        return $this->render(
            'product/edit.html.twig',
            [
            'product' => $product,
            'formView' => $formView
            ]
        );
    }

    /**
     * Suppression d'un produit
     * @Route("/admin/product/{id}/delete",name="admin_product_delete", requirements={"id": "\d+"}, methods="POST")
     * @param Product $product
     * @param Request $request
     */
    public function delete(
        Product $product,
        Request $request
    ) {
        if (!$product) {
            throw $this->createNotFoundException("le produit demandée n'éxiste pas");
        }
        if (
            $this->csrfTokenManagerInterface->isTokenValid(
                new CsrfToken("delete-product", $request->request->get('token'))
            )
        ) {
            $productName = $product->getName();
            $this->em->remove($product);
            $this->em->flush();

            $this->addFlash('success', "le produit " . $productName . " à bien été supprimé");
            return $this->redirectToRoute('admin_home');
        }
    }
}
