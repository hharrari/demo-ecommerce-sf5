<?php

namespace App\Controller\Admin\Category;

use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class AdminCategoryController extends AbstractController
{
    /**
    * @var EntityManager $em
    */
    protected $em;

    /**
     * @var CsrfTokenManagerInterface $csrfTokenManagerInterface
     */
    protected $csrfTokenManagerInterface;

    /**
     * @param EntityManagerInterface $em
     * @param CsrfTokenManagerInterface $csrfTokenManagerInterface
     */
    public function __construct(
        EntityManagerInterface $em,
        CsrfTokenManagerInterface $csrfTokenManagerInterface
    ) {
        $this->em = $em;
        $this->csrfTokenManagerInterface = $csrfTokenManagerInterface;
    }

    /**
     * Création d'une category
     *
     * @Route("/admin/category/create", name="admin_category_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category->setOwner($this->getUser());
            $this->em->persist($category);
            $this->em->flush();
            $this->addFlash('success', 'La categorie ' . $category->getName() . " à bien été créer");
            return $this->redirectToRoute('admin_home');
        }

        $formView = $form->createView();

        return $this->render(
            'category/create.html.twig',
            [
            'formView' => $formView
            ]
        );
    }

    /**
     * Modification d'une catégorie
     *
     * @Route("/admin/category/{id}/edit", name="admin_category_edit")
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function edit(Category $category, Request $request): Response
    {
        if (!$category) {
            throw $this->createNotFoundException("le produit demandée n'éxiste pas");
        }

        $this->denyAccessUnlessGranted('CAN_EDIT', $category, "Vous n'etes pas le propriétaire de cette catégorie");

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', "La catégorie " . $category->getName() . " à bien été modifié");
            return $this->redirectToRoute('admin_home');
        }

        $formView = $form->createView();

        return $this->render(
            'category/edit.html.twig',
            [
            'category' => $category,
            'formView' => $formView
            ]
        );
    }

    /**
     * Suppression d'une category
     * @Route("/admin/category/{id}/delete",name="admin_category_delete", requirements={"id": "\d+"}, methods="POST")
     * @param Category $category
     * @param Request $request
     */
    public function delete(
        Category $category,
        Request $request
    ) {
        if (!$category) {
            return $this->createNotFoundException("le produit demandée n'éxiste pas");
        }
        if (
            $this->csrfTokenManagerInterface->isTokenValid(
                new CsrfToken("delete-category", $request->request->get('token'))
            )
        ) {
            $this->em->remove($category);
            $this->em->flush();

            $this->addFlash('success', "le produit à bien été supprimé");
            return $this->redirectToRoute('admin_home');
        }
    }
}
