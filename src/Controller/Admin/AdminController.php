<?php

namespace App\Controller\Admin;

use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use App\Repository\PurchaseRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * DashBord access contents
     * @Route("/admin", name="admin_home")
     * @param ProductRepository $productRepository
     * @param CategoryRepository $categoryRepository
     * @param PurchaseRepository $purchaseRepository
     * @return Response
     */
    public function index(
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository,
        PurchaseRepository $purchaseRepository
    ): Response {
        $products = $productRepository->findby([], ['id' => 'DESC']);
        $categories = $categoryRepository->findby([], ['id' => 'DESC']);
        $purchases = $purchaseRepository->findBy([], ['id' => 'DESC']);

        return $this->render('admin/index.html.twig', [
            'products' => $products,
            'categories' => $categories,
            'purchases' => $purchases
        ]);
    }
}
