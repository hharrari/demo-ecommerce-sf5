<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use App\Event\ProductViewCounterEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    protected $dispatcher;

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        EventDispatcherInterface $dispatcher
    ) {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @Route("/{category_slug}/{slug}", name="product_show", priority=-1)
     * @param [string]                 $slug
     * @param ProductRepository        $productRepository
     * @return Response
     */
    public function show($slug, ProductRepository $productRepository): Response
    {
        $product = $productRepository->findOneBy(["slug" => $slug]);

        if (!$product) {
            throw $this->createNotFoundException("le produit demandée n'éxiste pas");
        }

        $this->dispatcher->dispatch(new ProductViewCounterEvent($product), 'product.view');

        return $this->render(
            'product/show.html.twig',
            [
            'product' => $product
            ]
        );
    }
}
