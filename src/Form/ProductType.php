<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Category;
use App\Form\DataTransformer\CentimesTransformer;
use App\Form\Type\PriceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                'label' => 'Nom du produit',
                'attr' => ['placeholder' => 'Tapez votre nom du produit']
                ]
            )
            ->add(
                'shortDescription',
                TextareaType::class,
                [
                'label' => 'Description du produit',
                'attr' => ['placeholder' => 'Tapez une description courte mais parlante pour le visiteur']
                ]
            )
            ->add(
                'price',
                MoneyType::class,
                [
                'label' => 'Description du produit',
                'attr' => ['placeholder' => 'Tapez le prix du produit en €'],
                'divisor' => 100
                ]
            )
            // Création de notre propre Type
            // ->add('price', PriceType::class, [
            //     'label' => 'Description du produit',
            //     'attr' => ['placeholder' => 'Tapez le prix du produit en €']
            // ])
            ->add(
                'mainPicture',
                TextType::class,
                [
                'label' => 'Image du produit',
                'attr' => ['placeholder' => "Url de l'image"]
                ]
            )
            ->add(
                'category',
                EntityType::class,
                [
                'label' => 'Choix de la catégorie',
                'placeholder' => '-- choisir une catégorie -- ',
                'class' => Category::class,
                'choice_label' => function (Category $category) {
                    return strtoupper($category->getName());
                }
                ]
            );

        // Les évenements de dataTransormer pour jouer sur les données du formulaire.

        // *****  Premier méthode ****
        // $builder->get('price')->addModelTransformer(new CallbackTransformer(
        //     // premier fonction sont la donnée reçu donc traitement avant affichage
        //     function ($value) {
        //         if ($value === null) {
        //             return;
        //         }
        //         return $value / 100;
        //     },
        //     // deuxième fonction sont la donnée envoyé du formulaire avant submit
        //     function ($value) {
        //         if ($value === null) {
        //             return;
        //         }
        //         return $value * 100;
        //     }
        // ));

        // ***** Deuxieme méthode *****
        // $builder->get('price')->addModelTransformer(new CentimesTransformer);

        // Les evenements utiles pour jouer sur la construction du formulaire
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();

                /**
            * @var Product
            */
                $product = $event->getData();
                // if ($product->getId() === null) {
                //     $form->add('category', EntityType::class, [
                //         'label' => 'Choix de la catégorie',
                //         'placeholder' => '-- choisir une catégorie -- ',
                //         'class' => Category::class,
                //         'choice_label' => function (Category $category) {
                //             return strtoupper($category->getName());
                //         }
                //     ]);
                // }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class' => Product::class,
            ]
        );
    }
}
