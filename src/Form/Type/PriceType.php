<?php

namespace App\Form\Type;

use App\Form\DataTransformer\CentimesTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceType extends AbstractType
{
    // On ajoute ici les actions
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['divide'] === false) {
            return;
        }

        $builder->addModelTransformer(new CentimesTransformer());
    }

    // Hérite de la configuration de type NumberType
    public function getParent()
    {
        return NumberType::class;
    }

    // On ajoute ici les options
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'divide' => true
            ]
        );
    }
}
