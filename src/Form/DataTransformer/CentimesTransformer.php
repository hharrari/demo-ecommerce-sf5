<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class CentimesTransformer implements DataTransformerInterface
{
    // premier fonction sont la donnée reçu donc traitement avant affichage

    /**
     * Transforme le prix en euro en base 10
     */
    public function transform($value)
    {
        if ($value === null) {
            return;
        }
        return $value / 100;
    }

    // deuxième fonction sont la donnée envoyé du formulaire avant submit
    /**
     * Tranforme le prix en centimes
     */
    public function reverseTransform($value)
    {
        if ($value === null) {
            return;
        }
        return $value * 100;
    }
}
