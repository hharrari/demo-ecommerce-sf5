<?php

namespace App\Doctrine\Listener;

use App\Doctrine\SluggerByNameTrait;
use App\Entity\Category;

class CategorySlugListener
{
    use SluggerByNameTrait;

    /**
     * @param Category $entity
     * @return void
     */
    public function prePersist(Category $entity)
    {
        if (empty($entity->getSlug())) {
            $this->setSlugByName($entity);
        }
    }

    /**
     * @param Category $entity
     * @return void
     */
    public function preFlush(Category $entity)
    {
        $this->setSlugByName($entity);
    }
}
