<?php

namespace App\Doctrine\Listener;

use App\Doctrine\SluggerByNameTrait;
use App\Entity\Product;

class ProductSlugListener
{
    use SluggerByNameTrait;

    public function prePersist(Product $entity)
    {
        if (empty($entity->getSlug())) {
            $this->setSlugByName($entity);
        }
    }

    public function preFlush(Product $entity)
    {
        $this->setSlugByName($entity);
    }
}
