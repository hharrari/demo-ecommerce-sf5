<?php

namespace App\Doctrine;

use Symfony\Component\String\Slugger\SluggerInterface;

trait SluggerByNameTrait
{
    /**
     * @var SluggerInterface
     */
    protected $slugger;

    /**
     * @param SluggerInterface $slugger
     */
    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    /**
     * Generate slug by name
     * @param [string] $entity
     * @return void
     */
    public function setSlugByName($entity)
    {
        $entity->setSlug(strtolower($this->slugger->slug($entity->getName())));
    }
}
