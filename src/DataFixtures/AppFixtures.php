<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Purchase;
use App\Entity\PurchaseItem;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    protected $slugger;
    protected $passwordHasher;

    public function __construct(
        SluggerInterface $slugger,
        UserPasswordHasherInterface $passwordHasher
    ) {
        $this->slugger = $slugger;
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
        $faker->addProvider(new \Bluemmb\Faker\PicsumPhotosProvider($faker));


        $admin = new User();
        $admin->setEmail("admin@gmail.com")
            ->setFullName("admin")
            ->setPassword($this->passwordHasher->hashPassword($admin, 'password'))
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        /**
         * Gestion des utilisateurs + création d'un array utilisateur pour gérer sur les entitées
         * en relation avec l'entité User et les afféctés par la suite
         */
        $users = [];
        for ($u = 0; $u < 5; $u++) {
            $user = new User();
            $user->setEmail("user$u@gmail.com")
                ->setFullName($faker->name())
                ->setPassword($this->passwordHasher->hashPassword($user, 'password'));

            $users[] = $user;

            $manager->persist($user);
        }

        for ($c = 0; $c < 3; $c++) {
            $category = new Category();
            $category->setName($faker->department())
                    ->setOwner($users[mt_rand(0, 4)]);

            $manager->persist($category);


            $products = [];
            for ($p = 0; $p < mt_rand(15, 20); $p++) {
                $product = new Product();

                $product->setName($faker->productName())
                    ->setPrice($faker->randomFloat(2, 4000, 20000))
                    ->setCategory($category)
                    ->setShortDescription($faker->paragraph())
                    ->setMainPicture($faker->imageUrl(800, 800, true));
                $products[] = $product;
                $manager->persist($product);
            }
        }

        for ($p = 0; $p < mt_rand(20, 40); $p++) {
            $purchase = new Purchase();
            $purchase->setFullName($faker->name)
                ->setAddress($faker->streetAddress())
                ->setPostalCode($faker->postcode())
                ->setCity($faker->city())
                // permet de lié UN utilisateur par commande relation ManyToOne par exemple
                ->setUser($faker->randomElement($users))
                ->setPurchasedAt($faker->dateTimeBetween('-6 months'));

            // permet de lié PLUSIEURS produits par commande relation ManyToMany par exemple
            $selectedProducts = $faker->randomElements($products, mt_rand(3, 5));
            $total = 0;
            foreach ($selectedProducts as $product) {
                // Création de ligne d'article dans une commande
                $purchaseItem = new PurchaseItem();
                $purchaseItem->setProduct($product)
                    ->setProductName($product->getName())
                    ->setProductPrice($product->getPrice())
                    ->setQuantity(mt_rand(1, 3))
                    ->setTotal(
                        $purchaseItem->getProductPrice() * $purchaseItem->getQuantity()
                    )
                    ->setPurchase($purchase);
                $total += $purchaseItem->getTotal();
                $manager->persist($purchaseItem);
            }

            $purchase->setTotal($total);

            if ($faker->boolean(90)) {
                $purchase->setStatus(Purchase::STATUS_PAID);
            }

            $manager->persist($purchase);
        }

        /**
        * Set data for test
        */

        $categoryTest = new Category();

        $categoryTest->setName('category Test')
        ->setOwner($admin)
        ->setSlug('category-test');

        $manager->persist($categoryTest);


        $productTest = new Product();

        $productTest->setName('product Test')
        ->setPrice(1233)
        ->setCategory($categoryTest)
        ->setMainPicture("https://fakeimg.pl/800x800/?text=Test")
        ->setShortDescription('Description product Test')
        ->setSlug('test-product');

        $manager->persist($productTest);

        $purchaseTest = new Purchase();

        $purchaseTest->setFullName('purchase Test')
        ->setAddress("1 avenue du puchase Test")
        ->setCity("Paris")
        ->setStatus(Purchase::STATUS_PAID)
        ->setPurchasedAt($faker->dateTimeBetween('-6 months'))
        ->setUser($users[0])
        ->setPostalCode("75000");

        $manager->persist($purchaseTest);

        $purchaseItemTest = new PurchaseItem();

        $purchaseItemTest->setProduct($productTest)
        ->setPurchase($purchaseTest)
        ->setProductName('product name test')
        ->setProductPrice($product->getPrice())
        ->setQuantity(12)
        ->setTotal($purchaseItemTest->getProductPrice() * $purchaseItem->getQuantity());

        $manager->persist($purchaseItemTest);


        $manager->flush();
    }
}
