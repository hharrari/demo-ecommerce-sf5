<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AmountFormattedExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('amount', [$this, 'amount']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('function_name', [$this, 'amount']),
        ];
    }

    public function amount(
        $value,
        string $symbol = '€',
        string $decsep = ',',
        string $thousandsep = ' '
    ) {
        // 12300
        $finalValue = $value / 100; // 123.00
        $finalValue = number_format($finalValue, 2, $decsep, $thousandsep); // 123,00

        return $finalValue . " " . $symbol; // 123,00 €
    }
}
